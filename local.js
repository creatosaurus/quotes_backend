const app = require('./index');
const logger = require('./logger');
// run the server locally
app.listen(process.env.PORT || 4007, () => {
    logger.info("Quotes running on port 4007")
})