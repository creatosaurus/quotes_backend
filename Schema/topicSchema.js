const mongoose = require('mongoose')

const topicSchema = mongoose.Schema({
    topic: {
        type: String,
        unique: true,
        trim: true,
        lowercase: true
    }
})

module.exports = mongoose.model("topic", topicSchema)